reaver (1.6.6-2) unstable; urgency=medium

  * d/control: Change Vcs-Git and Vcs-Browser.

 -- Leandro Cunha <leandrocunha016@gmail.com>  Fri, 01 Sep 2023 09:03:12 -0300

reaver (1.6.6-1) unstable; urgency=medium

  * New maintainer (change to Debian Security Tools Team). (Closes: #1040625)
  * d/control:
    - Set uploaders to Bartosz Fenski, Leandro Cunha and Samuel Henrique.
    - Update debhelper-compat to 13, this causes d/compat removal.
    - Bump Standards-Version to 4.6.2.
    - Set Vcs-Git and Vcs-Browser to Salsa repository with d/salsa-ci.yml.
    - Fix Lintian report change WiFi to Wi-Fi in description.
    - Set Rules-Requires-Root: no in the source stanza.
    - Declare Wash binary in description.
  * d/copyright:
    - Fix copyright format URI with HTTPS.
    - Add myself.
    - Change upstream name to reaver-wps-fork-t6x.
    - Update license files (fix Lintian warn).
    - Add public-domain license text.
    - Change BSD-3-clause license text.
    - Change GPL-2 license text and set GPL-2. No GPL-2 or later.
  * d/upstream/metadata created.
  * Fix Lintian report trailing whitespace in d/control, d/rules and
    d/changelog.
  * d/rules:
    - Export DEB_BUILD_MAINT_OPTIONS (fix Lintian and BLHC reports):
      - See https://wiki.debian.org/Hardening.
    - Remove override_dh_auto_install.
    - Add execute_after_dh_auto_clean for remove files generated during build:
      - Fix fails to build source after successful build. (Closes: #1046889)

 -- Leandro Cunha <leandrocunha016@gmail.com>  Mon, 28 Aug 2023 05:49:09 -0300

reaver (1.6.6-0.1) unstable; urgency=high

  * Non-maintainer upload.
  * New upstream version.
  * debian/watch:
    - Fix watch file is broken and generating errors. (Closes: #901595)
    - Change version of 3 to 4.
  * Fix segmentation fault. (Closes: #1036591)

 -- Leandro Cunha <leandrocunha016@gmail.com>  Sat, 27 May 2023 14:24:29 -0300

reaver (1.6.5-1) unstable; urgency=low

  * New upstream (forked) version (Closes: #898899)
  * Conf dir moved to /var/lib/reaver
    It's now also created during install (Closes: #720516, #720530)
  * Manpage should be up-to-date (Closes: #742170)
  * Bunch of changes to make lintian happy, standards-version up-to-date,
    and in general refresh package after 6 years of stagnation.

 -- Bartosz Fenski <fenio@debian.org>  Thu, 17 May 2018 11:35:41 +0200

reaver (1.4-2) unstable; urgency=low

  * Added missed copyright notes.

 -- Bartosz Fenski <fenio@debian.org>  Tue, 21 Feb 2012 18:36:48 +0100

reaver (1.4-1) unstable; urgency=low

  * New upstream version.
  * Includes wash binary and some additional README files.

 -- Bartosz Fenski <fenio@debian.org>  Mon, 13 Feb 2012 07:11:32 +0100

reaver (1.3-1) unstable; urgency=low

  * Initial release (Closes: #653758)

 -- Bartosz Fenski <fenio@debian.org>  Sat, 31 Dec 2011 09:14:38 +0100
